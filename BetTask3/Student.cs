﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetTask3
{
    class Student : Person
    {
        private int id;
        private int yearOfStudy;
        public int Id
        {
            get { return id; }
            set
            {
                if (id > 0)
                {
                    id = value;
                }
            }
        }
        public int YearOfStudy
        {
            get { return yearOfStudy; }
            set
            {
                if (yearOfStudy > 0 && yearOfStudy < 5)
                {
                    yearOfStudy = value;
                }
            }
        }
        public Student():base()
        {

        }
        public Student(int Id,int YearOfStudy,int age,string name) : base(name,age)
        {
            id = Id;
            yearOfStudy = YearOfStudy;
        }
       
    }
}

