﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetTask3
{
    class Person
    {
        private int age;
        private string name;
        public int Age
        {
            get { return age; }
            set
            {
                if (age > 0 && age < 100)
                    age = value;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name!=null)
                {
                    name = value;
                }
            }
        }
        public Person(string Name,int Age)
        {
            name = Name;
            age = Age;
        }
        public Person()
        {

        }
        public void Display()
        {
            Console.WriteLine($"name={name},age={age}");
        }
    }       
}
