﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetTask3
{
    class Program
    {
       
        static void Main(string[] args)
        {
            Student s = new Student(5, 2, 20, "Hovik");
            Teacher t = new Teacher("Papikyan", 35, "eng");
            Console.WriteLine($"id={s.Id},YearOfStudy,{s.YearOfStudy},age={s.Age},name={s.Name}");
            Console.WriteLine($"name={t.Name},age={t.Age},profession={t.Profession}");

        }
    }
}
