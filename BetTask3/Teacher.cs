﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetTask3
{
    class Teacher:Person
    {
        private string profession;
        public Teacher():base()
        {

        }
        public Teacher(string name,int age,string Profession):base(name, age)
        {
            profession = Profession;
           
        }
        public string Profession
        {
            get { return profession; }
            set { profession = value; }
        }
        
    }
}
